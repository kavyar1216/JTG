package com.travel.jtg;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

public class ReportGeneratorTest {
	List<Passenger> arrayList;
	ReportGenerator reportGenerator;

	@Before
	public void setUp() throws Exception {
		reportGenerator = new ReportGenerator();
		
		arrayList = new ArrayList<Passenger>();
		
		Passenger passenger1 = new Passenger(1,10,1,true,false,"Vacationer");
		Passenger passenger2 = new Passenger(2,199,2,true,false, "Vacationer");
		Passenger passenger3 = new Passenger(3, 99,0, false, true,"Commuter");
		Passenger passenger4 = new Passenger(5,155,0, false, true,"Commuter");
		Passenger passenger5 = new Passenger(4,199, 0, false, true,"Commuter");
		arrayList.add(passenger1);
		arrayList.add(passenger2);
		arrayList.add(passenger3);
		arrayList.add(passenger4);
		arrayList.add(passenger5);


	}

	@Test
	public void reportGeneratorTotalNewsPaperTest() {
		int expectedValue= 2;
		int actualValue=reportGenerator.numberOfNewsPapers(arrayList);
		assertEquals(expectedValue,actualValue);
		
		
	}
	
	@Test
	public void reportGeneratorNumberOfMealsTest() {
		double expectedValue= 3;
		double actualValue=reportGenerator.numberOfMeals(arrayList);
		assertEquals(expectedValue,actualValue,0.01);
		
		
	}
/*	@Test
	public void reportGeneratorTotalCostTest() {
		double expectedValue= 2;
		double actualValue=reportGenerator.totalCostOfTrip(arrayList);
		assertNotEquals(expectedValue,actualValue,0.01);
		
		
	}
*/
}
