package com.travel.jtg;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CommuterPassengersNewsPaperCount {

	private NewspaperClass newspaperClass;
	private Passenger passenger;
	@Before
	public void setUp() throws Exception {
	 newspaperClass = new NewspaperClass();
	 passenger = new Passenger(1,185,1,true,false,"Commuter");

	}

	@Test
	public void countNewsPapersTest() {
		
		boolean expectedValue=true;
		boolean actualValue = newspaperClass.countPapers(passenger);
		assertEquals(expectedValue,actualValue);
		
		
		
	}

}
