/*package com.travel.jtg;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class CommuterPassengersTest {
		
		private double expectedResult;
		private  Passenger passenger;
		private   CommutersPassengerClass commuterPassenger;
		
		public CommuterPassengersTest( double expectedResult ,Passenger passenger ) 
		{	super();
			this.expectedResult = expectedResult;
			this.passenger = passenger;
		}
		
		@Before
		public void setUp()  {
			commuterPassenger=new CommutersPassengerClass();
		}
		
		@Parameterized.Parameters
		public static Collection shippingCost() {
			return Arrays.asList(new Object[][] {
				{0.15, (new Passenger(3,90,1,false,true,"Commuter"))},
				{0.25, (new Passenger(5,90,1,false,true,"Commuter"))},
				{2,(new Passenger(4,199,0,true,false,"Commuter"))}, 
			});
		}
		
		
		
		@Test
		public void totalCostTest() {
			
			double actual = commuterPassenger.totalCost(passenger);
			assertEquals(expectedResult, actual, 0.001);
		}
		

	}

	







*/