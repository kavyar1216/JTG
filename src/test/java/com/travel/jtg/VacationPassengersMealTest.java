package com.travel.jtg;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class VacationPassengersMealTest {
	
	private VacationersMeal vacationersMeal;
	private Passenger passenger;
	
	@Before
	public void setUp() throws Exception {
		vacationersMeal=new VacationersMeal();
		passenger= new Passenger(1,185,1,false,false,"Vacationer");

	}
	
	@Test
	public void VacationerMealTest()
	{
		int expectedValue=2;
		double actualValue=vacationersMeal.Meals(passenger);
		assertEquals(expectedValue,actualValue,0.01);
		

	}
	
	

}
