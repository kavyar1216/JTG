package com.travel.jtg;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.travel.jtg.*;




@RunWith(Parameterized.class)
public class VacationPassengersTest {
	
	private float expectedResult;
	private  Passenger passenger;
	private   VacationPassengerClass vacationPassenger;
	
	public VacationPassengersTest( float expectedResult ,Passenger passenger ) 
	{	super();
		this.expectedResult = expectedResult;
		this.passenger = passenger;
	}
	
	@Before
	public void setUp() throws Exception {
	 vacationPassenger=new VacationPassengerClass();
	}
	
	@Parameterized.Parameters
	public static Collection shippingCost() {
		return Arrays.asList(new Object[][] {
			
			{45.0f, (new Passenger(1,90,1,false,false,"Vacationer"))},
			{99.5f,(new Passenger(1,199,0,true,false,"Vacationer"))}, 
		});
	}
	
	
	
	@Test
	public void totalCostTest() {
		
		double actual = vacationPassenger.totalCost(passenger);
		assertEquals(expectedResult, actual, 0.001);
	}
	
	
}
