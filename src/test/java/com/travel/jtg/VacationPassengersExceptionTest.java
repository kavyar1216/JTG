package com.travel.jtg;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class VacationPassengersExceptionTest {

	private  Passenger commutersPojo;
	private PassengerInterface passenger;
	private   VacationPassengerClass vacationPassenger;
	@Before
	public void setUp() throws Exception {
		 vacationPassenger=new VacationPassengerClass();

	}

	@Rule public ExpectedException thrown = ExpectedException.none();
	@Test
	public void totalCostTest_Exception() {
			Passenger commutersPojo = new Passenger(0, 3, 1, true, false, "Vacationer");
			thrown.expect(IllegalArgumentException.class);
			thrown.expectMessage("constraint voilated");
			vacationPassenger.totalCost(commutersPojo);
			}
	}


