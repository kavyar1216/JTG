package com.travel.jtg;

public class Passenger {
	private int stops;
	private double miles;
	private double meals;
	private boolean newspapers;
	private boolean frequentRider;
	private String type;
	
	public Passenger(int stops, int miles, int meals, boolean newspapers, boolean frequentRider,String type) {
		this.stops = stops;
		this.miles = miles;
		this.meals = meals;
		this.newspapers = newspapers;
		this.frequentRider = frequentRider;
		this.type=type;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public boolean getNewspapers() {
		return newspapers;
	}
	public void setNewspapers(boolean newspapers) {
		this.newspapers = newspapers;
	}
	public int getStops() {
		return stops;
	}
	
	public void setStops(int stops) {
		this.stops = stops;
	}
	public double getMiles() {
		return miles;
	}
	public void setMiles(double miles) {
		this.miles = miles;
	}
	public double getMeals() {
		return meals;
	}
	public void setMeals(double meals) {
		this.meals = meals;
	}
	public boolean isFrequentRider() {
		return frequentRider;
	}
	public void setFrequentRider(boolean frequentRider) {
		this.frequentRider = frequentRider;
	}

}
