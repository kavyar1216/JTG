package com.travel.jtg;

public enum RateFactor {
	
	    RATEFACTOR(0.5f);
		
	    public float Value;
	    
	    private RateFactor( float value)
	    {
	         Value = value;
	        
	    }
	    
	    public float getValue()
	    {
	    	return Value;
	    }
	}