package com.travel.jtg;

import java.util.List;

public class ReportGenerator {

	List<Passenger> passenger;
	VacationPassengerClass vacationPassengerClass;
	CommutersPassengerClass commutersPassengerClass;
	VacationersMeal vacationerMeal;
		int i;
		double j,k,m,l;
		
		public int numberOfNewsPapers(List<Passenger> passenger)
		{
			for(Passenger p: passenger)
			{
				if(p.getNewspapers())
				{
					i++;
				}
			}
			return i;
		}
	public double numberOfMeals(List<Passenger> passenger)
			{
		vacationerMeal= new VacationersMeal();
				for(Passenger p:passenger)
				{
					if(p.getMeals()>0)
					{
						j= vacationerMeal.Meals(p);
						k+=j;
					}
					
				}
				return k;
			}

	public double totalCostOfTrip(List<Passenger> passengers) {
		 vacationPassengerClass= new VacationPassengerClass();
		 commutersPassengerClass= new CommutersPassengerClass();
			for(Passenger p:passengers)
			{
				if(p.getType().equalsIgnoreCase("Vacationer"))
				{
				double l= vacationPassengerClass.totalCost(p);
				}
				else
				{
					double l= commutersPassengerClass.totalCost(p);
				}
				
				m+=l;
			}
			return m;
		}

	}

